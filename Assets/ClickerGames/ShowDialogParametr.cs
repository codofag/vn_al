// This code is part of the Fungus library (https://github.com/snozbot/fungus)
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;
using UnityEngine.Serialization;

namespace Fungus
{
    /// <summary>
    /// Waits for period of time before executing the next command in the block.
    /// </summary>
    [CommandInfo("Variable",
                 "Variable Show",
                 "Waits for period of time before executing the next command in the block.")]
    [AddComponentMenu("")]
    [ExecuteInEditMode]
    public class ShowDialogParametr : Command
    {
        [Tooltip("Duration to wait for")]
        [SerializeField] protected IntegerData value = new IntegerData(0);
        [SerializeField] protected StringData NameValue = new StringData();

        protected virtual void OnWaitComplete()
        {

        }

        #region Public members

        public override void OnEnter()
        {
            Flowchart flowchart = ParentBlock.GetFlowchart();
            int val = flowchart.GetIntegerVariable(NameValue);
            flowchart.SetIntegerVariable(NameValue, val + value.Value);
            iCore.ShowParametrPlus(NameValue, value.Value);
            Continue();
        }

        public override string GetSummary()
        {
            return NameValue;
        }

        public override Color GetButtonColor()
        {
            return new Color32(235, 191, 217, 255);
        }

        public override bool HasReference(Variable variable)
        {
            return value.integerRef == variable || base.HasReference(variable);
        }

        #endregion

        #region Backwards compatibility

        [HideInInspector] [FormerlySerializedAs("duration")] public int durationOLD;

        protected virtual void OnEnable()
        {
            if (durationOLD != default(float))
            {
                value.Value = durationOLD;
                durationOLD = default(int);
            }
        }

        #endregion
    }
}