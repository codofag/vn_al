using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ParametrDialog : MonoBehaviour
{
    public Text NameVariable;
    Animator animator;
    public List<GameObject> list;
    // Start is called before the first frame update
    void Start()
    {
        iCore.dialogParametr = this;
        animator = GetComponent<Animator>();
    }
    
    public void ShowParametr(string text, int value)
    {
        NameVariable.text = text;
        animator.Play("HideDialog");
        animator.Play("Show");

        foreach (var item in list)
        {
            item.SetActive(false);
        }
        if (value > 0)
        {
            list[0].SetActive(true);
        }
        if (value < 0)
        {
            list[1].SetActive(true);
        }

        if (value == 0)
        {
            list[2].SetActive(true);
        }
    }
}
