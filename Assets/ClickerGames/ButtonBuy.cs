using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBuy : MonoBehaviour
{
    public Text price;
    public Text priceB;
    public int priceBi,curP;
    public GameObject crystal;
    public GameObject bay;
    public ShangeButton ShangeButton;
    Image ImageButton;
    public Wardrobe wardrobe;
public static bool paid;
    public Sprite freeButton;
    public Button btn;
    public Sprite priceButton;
    public Flowchart global;
    iItem newItem;
    public void ShowButton(iItem item = null)
    {
        
        int countCrystal = 0;

        //foreach (var variable in global.Variables)
        //{
        //    if (variable.Key == "crystal")
        //    {
        //        countCrystal = (variable as IntegerVariable).Value;
        //        paid = true;
        //    }
        //    else
        //    {

        //    }


        //}

        paid = false; 
        newItem = item;
        ImageButton = GetComponent<Image>();
        

        bool result = false;

        if (item != null)
        {

            result = item.price > 0 ? !wardrobe.FindItem(item) : false;
            price.text = item.price.ToString();
            int.TryParse(price.text, out curP);
            
            if (item.price >  curP && result)
            {
                //Button.interactable = false;
            }
            
        }


        ImageButton.sprite = result ? priceButton : freeButton;
        
        if (crystal != null)
        {
            if (result)
            { 
                int.TryParse(price.text, out curP);
                {
                    
                }
                if(priceBi-curP>=0)
                {
                ///ShangeButton.shd=true;
                bay.SetActive(true);
                crystal.SetActive(result);
                ///ShangeButton.shd=true;
                var Button = GetComponent<Button>();
        Button.interactable = true;
                }
                else
                {
                ///ShangeButton.shd=true;
                bay.SetActive(true);
                crystal.SetActive(result);
                ///ShangeButton.shd=true; 
                var Button = GetComponent<Button>();
        Button.interactable = false; 
                }
                
            }
            else
            {
                crystal.SetActive(false);
                price.text = "Выбрать";
                
                bay.SetActive(false);
            }
                        
        }
        else
        {
            price.gameObject.SetActive(result);
        }
    }

    public void onClick()
    {


        Debug.Log(newItem);


        if (newItem != null)
        {
            wardrobe.addItem(newItem);
           
            foreach (var item in global.Variables)
            {  
                if(price.text != "Выбрать")
            {
                if (item.Key == "crystal")
                {  
                    int.TryParse(price.text, out curP);
                    Debug.Log(curP);
                    priceBi =priceBi-curP;
                    //priceB.text=priceBi.ToString();
                    (item as IntegerVariable).Value -= newItem.price;
                    global.FindBlock("Crystal").StartExecution();
                }
            }
            }
            
        }

        
    }



    public void Update()
    {
        foreach (var item in global.Variables)
        {
            if (item.Key == "crystal")
            {

                priceBi = (item as IntegerVariable).Value;

            } 
        
        }

    }
}


