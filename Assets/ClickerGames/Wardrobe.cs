using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wardrobe : MonoBehaviour
{
    bool show = false;
    public GameObject Panel;
    public Flowchart fungus;
    public HairPanel hairPanel;

    public GameObject LeftButton, RightButton;

    public float musicFungus {get { return GetMusic(); } set {SetMusic(value); } }
    public float music = 1;

    float GetMusic()
    {        
        return FungusManager.Instance.MusicManager.audioSourceMusic.volume;
    }

    void SetMusic(float volume)
    {
        FungusManager.Instance.MusicManager.audioSourceMusic.volume = volume;
    }



    public void Update()
    {
        musicFungus = music;
    }

    public iData GameData;

    public Image Body;
    public Image Robe;
    public Image Jewelry;
    public Image Hair;

    public Image GameBody;
    public Image GameRobe;
    public Image GameJewelry;
    public Image GameHair;


    List<iItem> BodyItems;
    List<iItem> RobeItems;
    List<iItem> JewelryItems;
    List<iItem> HairItems;

    int numGroup;

    int BodyIndex;
    int RobeIndex;
    int JewelryIndex;
    int HairIndex;
    int Hairnum;

    private void Start()
    {

        iCore.wardrobe = this;
        
    }
    
    public void addItem(iItem item)
    {
        if (!FindItem(item) && item != null)
        {
            Debug.Log(item.type);
            GameData.IdItems.Add(item);
            if (item.type != RItem.TypeItem.Any)
            {
                if (item.type == RItem.TypeItem.Body)
                {
                    ++BodyIndex;
                    GameData.indexBody = BodyIndex;
                    GameData.selectBody = item;
                }

                if (item.type == RItem.TypeItem.Hair)
                {
                    ++HairIndex;
                    GameData.indexHair = HairIndex;
                    GameData.selectHair = item;
                }

                if (item.type == RItem.TypeItem.Jewelry)
                {
                    ++JewelryIndex;
                    GameData.indexJewelry = JewelryIndex;
                    GameData.selectJewelry = item;
                }

                if (item.type == RItem.TypeItem.Robe)
                {
                    ++RobeIndex;
                    GameData.indexRobe = RobeIndex;
                    GameData.selectRobe = item;
                    
                }

                Reload();
                //click_OK();
            }
            
        }
        fungus.gameObject.GetComponent<Controller>().SaveGame();
    }


    public bool FindItem(iItem findItem)
    {
        foreach (var item in GameData.IdItems)
        {
            if (findItem.id == item.id)
            {
                return true;
            }
        }

        return false;
    }


    public void Load(iData data)
    {
        GameData = data;
        Reload();

        //BodyIndex = GameData.indexBody;
        //RobeIndex = GameData.indexRobe;
        //JewelryIndex = GameData.indexJewelry;
        //HairIndex = GameData.indexHair;

        Hairnum = GameData.numHair;
        
        ShowPerson();
    }


    public void Reload()
    {
        Debug.Log("Reload");
        BodyItems = new List<iItem>();
        RobeItems = new List<iItem>();
        JewelryItems = new List<iItem>();
        HairItems = new List<iItem>();
        Hairnum = GameData.numHair;
        hairPanel.hairSelect(Hairnum);

        int b = 0;
        int r = 0;
        int j = 0;
        int h = 0;
        foreach (var item in GameData.IdItems)
        {
            if (item.type == RItem.TypeItem.Body)
            {
                BodyItems.Add(item);
                if (item.id == GameData.selectBody.id)
                {
                    BodyIndex = b;
                }
                b++;
            }
            
            if (item.type == RItem.TypeItem.Robe)
            {
                RobeItems.Add(item);
                if (item.id == GameData.selectRobe.id)
                {
                    RobeIndex = r;
                }
                r++;
            }

            if (item.type == RItem.TypeItem.Jewelry)
            {
                JewelryItems.Add(item);
                if (item.id == GameData.selectJewelry.id)
                {
                    JewelryIndex = j;
                }
                j++;
            }

            if (item.type == RItem.TypeItem.Hair)
            {
                HairItems.Add(item);
                if(item.id == GameData.selectHair.id)
                {
                    HairIndex = h;
                }
                h++;
            }

        }
        ShowHideLeftRightButtons();

    }

    public void SelectNewItem(iItem item)
    {
        if (item.type == RItem.TypeItem.Body)
        {
            GameBody.sprite = item.sprite[0];
        }

        if (item.type == RItem.TypeItem.Robe)
        {
            GameRobe.sprite = item.sprite[0];
        }

        if (item.type == RItem.TypeItem.Jewelry)
        {
            GameJewelry.sprite = item.sprite[0];
        }

        if (item.type == RItem.TypeItem.Hair)
        {
            GameHair.sprite = item.sprite[0];
        }
    }

    public void ShowPerson()
    {
        Body.sprite =        GameData.selectBody.sprite[0];
        Robe.sprite =        GameData.selectRobe.sprite[0];
        Jewelry.sprite =     GameData.selectJewelry.sprite[0];
        Hair.sprite =        GameData.selectHair.sprite[GameData.numHair];

        GameBody.sprite =    GameData.selectBody.sprite[0];
        GameRobe.sprite =    GameData.selectRobe.sprite[0];
        GameJewelry.sprite = GameData.selectJewelry.sprite[0];
        GameHair.sprite =    GameData.selectHair.sprite[GameData.numHair];
    }

    public void PreShowPerson()
    {
        if (Hairnum >= HairItems[HairIndex].sprite.Count)
        {
            Hairnum = 0;
            hairPanel.hairSelect(Hairnum);
        }

        Body.sprite =    BodyItems[BodyIndex].sprite[0];
        Robe.sprite =    RobeItems[RobeIndex].sprite[0];
        Jewelry.sprite = JewelryItems[JewelryIndex].sprite[0];
        Hair.sprite =    HairItems[HairIndex].sprite[Hairnum];

        ShowHairPanel();
    }

    public void ShowHairPanel()
    {
        hairPanel.ShowButtons(HairItems[HairIndex]);
    }

    public void ChangeHairNum(int num)
    {
        Hairnum = num;
        PreShowPerson();

    }

    public void click_OK()
    {
        GameData.selectBody = BodyItems[BodyIndex];
        GameData.selectRobe = RobeItems[RobeIndex];
        GameData.selectJewelry = JewelryItems[JewelryIndex];
        GameData.selectHair = HairItems[HairIndex];
        GameData.numHair    = Hairnum;
        hairPanel.gameObject.SetActive(false);
        ShowPerson();
        fungus.gameObject.GetComponent<Controller>().SaveGame();
        

    }

    public void ChangeGroup(int num)
    {
        numGroup = num;


        ShowHideLeftRightButtons();
    }

    void ShowHideLeftRightButtons()
    {
        bool result = false;

        if (numGroup == 0)
        {
            if (BodyItems.Count > 1)
            {
                result = true;
            }
        }

        if (numGroup == 1)
        {
            if (RobeItems.Count > 1)
            {
                result = true;
            }
        }

        if (numGroup == 2)
        {
            if (JewelryItems.Count > 1)
            {
                result = true;
            }
        }

        if (numGroup == 3)
        {
            if (HairItems.Count > 0)
            {
                result = true;
            }
        }
        LeftButton.SetActive(result);
        RightButton.SetActive(result);
        if (numGroup == 3)
        {
            hairPanel.gameObject.SetActive(true);
            ShowHairPanel();
        }
        else
        {
            hairPanel.gameObject.SetActive(false);
        }

    }

    public void Left()
    {
        if (numGroup == 0)
        {
            BodyIndex--;
            if (BodyIndex < 0)
            {
                BodyIndex = BodyItems.Count - 1;
            }
        }

        if (numGroup == 1)
        {
            RobeIndex--;
            if (RobeIndex < 0)
            {
                RobeIndex = RobeItems.Count - 1;
            }
        }

        if (numGroup == 2)
        {
            JewelryIndex--;
            if (JewelryIndex < 0)
            {
                JewelryIndex = JewelryItems.Count - 1;
            }
        }

        if (numGroup == 3)
        {
            HairIndex--;
            if (HairIndex < 0)
            {
                HairIndex = HairItems.Count - 1;
            }
        }



        PreShowPerson();
    }

    public void Right()
    {
        if (numGroup == 0)
        {
            BodyIndex++;
            if (BodyIndex == BodyItems.Count)
            {
                BodyIndex = 0;
            }
        }

        if (numGroup == 1)
        {
            RobeIndex++;
            if (RobeIndex == RobeItems.Count)
            {
                RobeIndex = 0;
            }
        }

        if (numGroup == 2)
        {
            JewelryIndex++;
            if (JewelryIndex == JewelryItems.Count)
            {
                JewelryIndex = 0;
            }
        }

        if (numGroup == 3)
        {
            HairIndex++;
            if (HairIndex == HairItems.Count)
            {
                HairIndex = 0;
            }
        }
        PreShowPerson();
    }



    public void SaveOldWardrobe()
    {
        GameData.oldSelectBody = GameData.selectBody;
        GameData.oldSelectHair = GameData.selectHair;
        GameData.oldSelectJewelry = GameData.selectJewelry;
        GameData.oldSelectRobe = GameData.selectRobe;

        
        
    }

    public void LoadOldWardrobe()
    {
        GameData.selectBody = GameData.oldSelectBody;
        GameData.selectHair = GameData.oldSelectHair;
        GameData.selectJewelry = GameData.oldSelectJewelry;
        GameData.selectRobe = GameData.oldSelectRobe;
        Reload();
        ShowPerson();
    }
}
