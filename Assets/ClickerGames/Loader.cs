using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{
    public string nameScene;
    public Image progressbar;
    public Image Dark;
    public Text text;
    float progres;
    public void Load()
    {
        //gameObject.SetActive(true);
        StartCoroutine(LoadYourAsyncScene(nameScene));
    }

void Update()
{
    if(progres<100)
    {
    progres= progres+0.2f; 
    text.text=Mathf.Abs( (int)(progres)).ToString()+"%";
    progressbar.fillAmount=progres/100;
    }
}
    IEnumerator LoadYourAsyncScene(string name)
    {
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name);
        asyncLoad.allowSceneActivation = false;
        // Wait until the asynchronous scene fully loads
        float i = 0;
        float progress = 0;
        float Timer = 0;
     
        //while (asyncLoad.progress < 0.9f || Timer < 2f)
            while (asyncLoad.progress < 0.9f || Timer < 2f)
            {
            if (asyncLoad.progress > 0.85f)
            {
                //progressbar.fillAmount = 1f;
                Timer += Time.deltaTime;
                Dark.color = new Color(0,0,0, Timer);
                var current = new Vector2(progressbar.fillAmount, 0);
                var target = new Vector2(1, 0);
                //progressbar.fillAmount = Vector2.MoveTowards(current, target, Vector2.Distance(current, target) / 50f).x;
                   
            }
            else
            {
                i += Time.deltaTime / 100f;
                progress = asyncLoad.progress / 4f + 0.75f;
                var current = new Vector2(progressbar.fillAmount, 0);
                var target = new Vector2(progress, 0);
                //Debug.Log(asyncLoad.progress);
                //progressbar.fillAmount = Vector2.MoveTowards(current, target, Vector2.Distance(current, target) / 100f).x;
               
            }
  
            yield return null;
        }

        asyncLoad.allowSceneActivation = true;
    }
}
