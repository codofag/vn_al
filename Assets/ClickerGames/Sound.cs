using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public void soundOn()
    {
        AudioListener.volume = 1f;
    }

    public void soundOff()
    {
        AudioListener.volume = 0f;
    }
}
