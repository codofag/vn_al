using Fungus;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class iCore
{
    public static ParametrDialog dialogParametr;
    public static Wardrobe wardrobe;
    public static List<GameObject> itemsList = new List<GameObject>();
    public static View CurrentView;

    public static void ShowParametrPlus(string name, int value)
    {
        dialogParametr.ShowParametr(name, value);
    }

    public static void  additems(GameObject obj)
    {
        itemsList.Add(obj);
    }

}
