using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using System;

[Serializable]
public class iData
{
    public string name;
    [Serializable]
    public class Episode
    {
        public int NumEpisode;
        public string Name;
        public string LastBlockName;
        public List<Variable> Variables;
    }

    public List<Episode> Episodes;
    public List<iItem> IdItems;

    public iItem selectBody;
    public iItem selectRobe;    
    public iItem selectJewelry;    
    public iItem selectHair;

    public iItem oldSelectBody;
    public iItem oldSelectRobe;
    public iItem oldSelectJewelry;
    public iItem oldSelectHair;

    public int indexBody;
    public int indexRobe;
    public int indexJewelry;
    public int indexHair;
    public int numHair;



}


