using Fungus;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public string NameNovella;
    public int numEpisode;
    public string NameEpisode;
    public iData GameData;

    public RItem Body;
    public RItem Robe;
    public RItem Jewelry;
    public RItem Hair;

    bool sound = true;

    public Wardrobe WARDROBE;

    public Flowchart global;
    



    public void LoadGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        //var json = JsonUtility.ToJson(GameData);
        string filePath = Application.persistentDataPath + $"/{NameNovella}.dat";
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);

            var json = (string)bf.Deserialize(file);
            GameData = JsonUtility.FromJson<iData>(json);
            file.Close();
            WARDROBE.Load(GameData);
        }
        else
        {
            NewEpisode();
        }

    }
    public void NewEpisode()
    {
       
        GameData = new iData
        {
            Episodes = new List<iData.Episode>
            {
                new iData.Episode
                {
                    NumEpisode = numEpisode,
                    Name = NameEpisode,
                    LastBlockName = "StartGame"
                }
            },

            IdItems = new List<iItem>
            {
                Body.GiveMeItem(),
                Robe.GiveMeItem(),
                Jewelry.GiveMeItem(),
                Hair.GiveMeItem()
            },
            selectBody = Body.GiveMeItem(),
            selectRobe = Robe.GiveMeItem(),
            selectJewelry = Jewelry.GiveMeItem(),
            selectHair = Hair.GiveMeItem()

        };

        WARDROBE.Load(GameData);

        Debug.Log($"New episode {NameEpisode} create");
    }
    public void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        var json = JsonUtility.ToJson(WARDROBE.GameData);
        FileStream file = File.Create(Application.persistentDataPath + $"/{NameNovella}.dat");
        bf.Serialize(file, json);
        file.Close();
    }

    public void soundOn()
    {
        sound = true;
        AudioListener.volume = 1f;
    }

    public void soundOff()
    {
        sound = false;
        AudioListener.volume = 0f;
    }

    public void minusCas()
    {
        foreach (var item in global.Variables)
        {
            if (item.Key == "cassette")
            {

                (item as IntegerVariable).Value -= 1;
                global.FindBlock("Cassete").StartExecution();

            }

        }

        
    }

}
