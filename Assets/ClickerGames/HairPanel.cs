using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HairPanel : MonoBehaviour
{
    public List<GameObject> buttons;
    public Wardrobe wardrobe;

    public Sprite select;
    public Sprite unSelect;
    public void ShowButtons(iItem hair)
    {
        foreach (var item in buttons)
        {
            item.SetActive(false);
        }

        for (int i = 0; i < hair.icons.Count; i++)
        {
            buttons[i].transform.GetChild(0).GetComponent<Image>().sprite = hair.icons[i];
            buttons[i].SetActive(true);
        }
    }

    public void buttonClick(int num)
    {
        hairSelect(num);

        wardrobe.ChangeHairNum(num);
    }

    public void hairSelect(int num)
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            if (num == i)
            {
                buttons[i].GetComponent<Image>().sprite = select;
            }
            else
            {
                buttons[i].GetComponent<Image>().sprite = unSelect;
            }
        }
    }
}
