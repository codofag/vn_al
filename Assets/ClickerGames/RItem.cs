using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RItem : MonoBehaviour
{
    //public int ID;
    public int Price;
    public List<Sprite> Sprites;
    public List<Sprite> Icons;
    public iItem item;
    [HideInInspector]
    public TypeItem Type = TypeItem.Any;
    
    public enum TypeItem
    {
        Body,
        Robe,
        Jewelry,
        Hair,
        Any
    }
    // Start is called before the first frame update
    void Start()
    {
        var parentName = gameObject.transform.parent.name;
        iCore.additems(gameObject);

        if (parentName == "Body")
        {
            Type = TypeItem.Body;
        }

        if (parentName == "Robe")
        {
            Type = TypeItem.Robe;
        }

        if (parentName == "Jewelry")
        {
            Type = TypeItem.Jewelry;
        }

        if (parentName == "Hair")
        {
            Type = TypeItem.Hair;
        }

        if (parentName == "Any")
        {
            Type = TypeItem.Any;
        }

        item = new iItem
        {
            id = gameObject.name,
            price = Price,
            sprite = Sprites,
            icons = Icons,
            type = Type,
            currentObject = gameObject
        };
    }

    public iItem GiveMeItem()
    {
        return item;
    }
}
[Serializable]
public class iItem
{
    public string id;
    public int price;
    public RItem.TypeItem type;
    public List<Sprite> sprite { get { return GiveMeSprites(); } set { Sprites = value; } }
    public List<Sprite> icons { get { return GiveMeIcons(); } set { Icons = value; } }


    [NonSerialized]
    public GameObject currentObject;
    [NonSerialized]
    public List<Sprite> Sprites;
    [NonSerialized]
    public List<Sprite> Icons;

    List<Sprite> GiveMeSprites()
    {

        if (currentObject != null)
        {
            return Sprites;
        }
        else
        {
            foreach (var item in iCore.itemsList)
            {
                if (item != null)
                if (item.name == id)
                {
                    currentObject = item;
                    Sprites = item.GetComponent<RItem>().GiveMeItem().sprite;
                    Icons = item.GetComponent<RItem>().GiveMeItem().icons;
                }
            }
        }

        return Sprites;
    }

    List<Sprite> GiveMeIcons()
    {

        if (currentObject != null)
        {
            return Icons;
        }
        else
        {
            foreach (var item in iCore.itemsList)
            {
                if (item != null)
                if (item.name == id)
                {
                    currentObject = item;
                    Sprites = item.GetComponent<RItem>().GiveMeItem().sprite;
                    Icons = item.GetComponent<RItem>().GiveMeItem().icons;
                }
            }
        }

        return Icons;
    }
}
