using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderMenu : MonoBehaviour
{
    public GameObject firstObject;
    GameObject secondaryObject;
    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void StartSlider(GameObject obj)
    {
        secondaryObject = obj;
        animator.Play("Slider");
    }

    void ChangeObjects()
    {
        firstObject.SetActive(false);
        secondaryObject.SetActive(true);

        firstObject = secondaryObject;
    }
}
