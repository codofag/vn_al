using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Swipe : MonoBehaviour
{

    public List<GameObject> objects;
    public ScrollRect scrollView;
    public GameObject content;
    float delta;
    // Update is called once per frame
    void Update()
    {
        
        var size = Mathf.Abs(content.transform.position.x) / (delta * 4f);
        objects[0].transform.localScale = new Vector2(1 - size, 1 - size);

        size = Mathf.Abs(content.transform.position.x) / (delta * 4f);
        objects[1].transform.localScale = new Vector2(size + 0.86f, size + 0.86f);
        size = Mathf.Abs((728f + content.transform.position.x) / 729f);
        size = size > 1f ? size : 1f;
        objects[0].transform.localScale = new Vector2(size, size);


    }
    public void Start()
    {
        delta = content.GetComponent<RectTransform>().sizeDelta.x;
    }
}
