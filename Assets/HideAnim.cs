using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Fungus
{
public class HideAnim : MonoBehaviour
{
    public Animator animator;
    public DialogInput dialog;
    // Start is called before the first frame update
   public void hv()
    {
        animator.SetBool("hide",true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private IEnumerator WaitAndHide(float waitTime)
    {
        
        {
            yield return new WaitForSeconds(waitTime);
           dialog.SetDialogClickedFlag();
        }
    }
    
}
}
