// This code is part of the Fungus library (https://github.com/snozbot/fungus)
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;

namespace Fungus
{
    /// <summary>
    /// Save an Boolean, Integer, Float or String variable to persistent storage using a string key.
    /// The value can be loaded again later using the Load Variable command. You can also 
    /// use the Set Save Profile command to manage separate save profiles for multiple players.
    /// </summary>
    [CommandInfo("Blocks",
                 "NextBlock", 
                 "Save an Boolean, Integer, Float or String variable to persistent storage using a string key. " +
                 "The value can be loaded again later using the Load Variable command. You can also " +
                 "use the Set Save Profile command to manage separate save profiles for multiple players.")]
    [AddComponentMenu("")]
    public class NextBlock : Command
    {
       // [Tooltip("Name of the saved value. Supports variable substition e.g. \"player_{$PlayerNumber}")]
        //[SerializeField] protected string key = "";
        
        [Tooltip("Variable to read the value from. Only Boolean, Integer, Float and String are supported.")]
        [VariableProperty(typeof(BooleanVariable),
                          typeof(IntegerVariable), 
                          typeof(FloatVariable), 
                          typeof(StringVariable))]
        [SerializeField] protected Variable variable;

        #region Public members

        public override void OnEnter()
        {
            if (variable == null)
            {
                Continue();
                return;
            }
            
            var flowchart = GetFlowchart();
            StringVariable stringVariable = variable as StringVariable;
            Debug.Log(stringVariable.Value);
            var block = flowchart.FindBlock(stringVariable.Value);
            block.StartExecution();
            Continue();
        }
        
        public override string GetSummary()
        {

            
            if (variable == null)
            {
                return "Error: No variable selected";
            }
            
            return variable.Key;
        }
        
        public override Color GetButtonColor()
        {
            return new Color32(235, 191, 217, 255);
        }

        public override bool HasReference(Variable in_variable)
        {
            return this.variable == in_variable || base.HasReference(in_variable);
        }

        #endregion
        #region Editor caches
#if UNITY_EDITOR
        protected override void RefreshVariableCache()
        {
            base.RefreshVariableCache();

            var f = GetFlowchart();

            f.DetermineSubstituteVariables("", referencedVariables);
        }
#endif
        #endregion Editor caches
    }
}