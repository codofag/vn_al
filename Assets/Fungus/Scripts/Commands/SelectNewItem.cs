// This code is part of the Fungus library (https://github.com/snozbot/fungus)
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;
using UnityEngine.Serialization;

namespace Fungus
{
    /// <summary>
    /// Waits for period of time before executing the next command in the block.
    /// </summary>
    [CommandInfo("Action",
                 "SelectNewItem", 
                 "Waits for period of time before executing the next command in the block.")]
    [AddComponentMenu("")]
    [ExecuteInEditMode]
    public class SelectNewItem : Command
    {
        [Tooltip("Duration to wait for")]
        [SerializeField] protected FloatData _duration = new FloatData(0);
        [SerializeField] protected RItem item;

        protected virtual void OnWaitComplete()
        {
            if (item != null)
            {
                iCore.wardrobe.SelectNewItem(item.item);
                if (item.Type == RItem.TypeItem.Robe)
                {
                    iCore.wardrobe.GameData.selectRobe = item.item;
                }
                if (item.Type == RItem.TypeItem.Hair)
                {
                    iCore.wardrobe.GameData.selectHair = item.item;
                }
                if (item.Type == RItem.TypeItem.Body)
                {
                    iCore.wardrobe.GameData.selectBody = item.item;
                }
                if (item.Type == RItem.TypeItem.Jewelry)
                {
                    iCore.wardrobe.GameData.selectJewelry = item.item;
                }



            }
            Continue();
        }

        #region Public members

        public override void OnEnter()
        {
            Invoke ("OnWaitComplete", _duration.Value);
        }

        public override string GetSummary()
        {
            return _duration.Value.ToString() + " seconds";
        }

        public override Color GetButtonColor()
        {
            return new Color32(235, 191, 217, 255);
        }

        public override bool HasReference(Variable variable)
        {
            return _duration.floatRef == variable || base.HasReference(variable);
        }

        #endregion

        #region Backwards compatibility

        [HideInInspector] [FormerlySerializedAs("duration")] public float durationOLD;

        protected virtual void OnEnable()
        {
            if (durationOLD != default(float))
            {
                _duration.Value = durationOLD;
                durationOLD = default(float);
            }
        }

        #endregion
    }
}