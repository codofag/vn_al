// This code is part of the Fungus library (https://github.com/snozbot/fungus)
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;

namespace Fungus
{
    /// <summary>
    /// Loads a saved value and stores it in a Boolean, Integer, Float or String variable. If the key is not found then the variable is not modified.
    /// </summary>
    [CommandInfo("Variable", 
                 "Load All Variable", 
                 "Loads a saved value and stores it in a Boolean, Integer, Float or String variable. If the key is not found then the variable is not modified.")]
    [AddComponentMenu("")]
    public class LoadAllVariable : Command
    {
        [Tooltip("Name of the saved value. Supports variable substition e.g. \"player_{$PlayerNumber}\"")]
        [SerializeField] protected string key = "";

        [Tooltip("Variable to store the value in.")]
        [VariableProperty(typeof(BooleanVariable),
                          typeof(IntegerVariable), 
                          typeof(FloatVariable), 
                          typeof(StringVariable))]
        [HideInInspector]
        [SerializeField] protected Variable variable;

        #region Public members

        public override void OnEnter()
        {
            if (key == "")
            {
                Continue();
                return;
            }

            var flowchart = GetFlowchart();
            foreach (var item in flowchart.Variables)
            {

                // Prepend the current save profile (if any)
                string prefsKey = key + "_" + flowchart.SubstituteVariables(item.Key);

                System.Type variableType = item.GetType();

                if (variableType == typeof(BooleanVariable))
                {
                    BooleanVariable booleanVariable = item as BooleanVariable;
                    if (booleanVariable != null)
                    {
                        // PlayerPrefs does not have bool accessors, so just use int
                       
                        booleanVariable.Value = (PlayerPrefs.GetInt(prefsKey) == 1);
                    }
                }
                else if (variableType == typeof(IntegerVariable))
                {
                    IntegerVariable integerVariable = item as IntegerVariable;
                    if (integerVariable != null)
                    {
                        integerVariable.Value = PlayerPrefs.GetInt(prefsKey, integerVariable.Value);
                    }
                }
                else if (variableType == typeof(FloatVariable))
                {
                    FloatVariable floatVariable = item as FloatVariable;
                    if (floatVariable != null)
                    {
                        floatVariable.Value = PlayerPrefs.GetFloat(prefsKey, floatVariable.Value);
                    }
                }
                else if (variableType == typeof(StringVariable))
                {
                    StringVariable stringVariable = item as StringVariable;
                    if (stringVariable != null)
                    {
                        var value = PlayerPrefs.GetString(prefsKey, stringVariable.Value);
                        if (value != "")
                        {
                            stringVariable.Value = value;
                        }
                        
                    }
                }
            }

            var controller = flowchart.gameObject.GetComponent<Controller>();
            if (controller != null)
            {
                controller.LoadGame();
            }

            Continue();
        }
        
        public override string GetSummary()
        {
            if (key.Length == 0)
            {
                return "Error: No stored value key selected";
            }       


            return  key ;
        }

        public override Color GetButtonColor()
        {
            return new Color32(235, 191, 217, 255);
        }

        public override bool HasReference(Variable in_variable)
        {
            return this.variable == in_variable ||
                base.HasReference(in_variable);
        }

        #endregion
        #region Editor caches
#if UNITY_EDITOR
        protected override void RefreshVariableCache()
        {
            base.RefreshVariableCache();

            var f = GetFlowchart();

            f.DetermineSubstituteVariables(key, referencedVariables);
        }
#endif
        #endregion Editor caches
    }
}