// This code is part of the Fungus library (https://github.com/snozbot/fungus)
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;

namespace Fungus
{
    /// <summary>
    /// Writes text in a dialog box.
    /// </summary>
    [CommandInfo("Narrative", 
                 "Say", 
                 "Writes text in a dialog box.")]
    [AddComponentMenu("")]
    public class Say : Command, ILocalizable
    {
        // Removed this tooltip as users's reported it obscures the text box
        [TextArea(5,10)]
        [SerializeField] protected string storyText = "";

        [Tooltip("Notes about this story text for other authors, localization, etc.")]
        [SerializeField] protected string description = "";

        [Tooltip("Character that is speaking")]
        [SerializeField] protected Character character;

        [Tooltip("Portrait that represents speaking character")]
        [SerializeField] protected Sprite portrait;

        [Tooltip("Voiceover audio to play when writing the text")]
        [SerializeField] protected AudioClip voiceOverClip;

        [Tooltip("Always show this Say text when the command is executed multiple times")]
        [SerializeField] protected bool showAlways = true;

        [Tooltip("Number of times to show this Say text when the command is executed multiple times")]
        [SerializeField] protected int showCount = 1;

        [Tooltip("Type this text in the previous dialog box.")]
        [SerializeField] protected bool extendPrevious = false;

        [Tooltip("Fade out the dialog box when writing has finished and not waiting for input.")]
        [SerializeField] protected bool fadeWhenDone = true;

        [Tooltip("Wait for player to click before continuing.")]
        [SerializeField] protected bool waitForClick = true;

        [Tooltip("Stop playing voiceover when text finishes writing.")]
        [SerializeField] protected bool stopVoiceover = true;

        [Tooltip("Wait for the Voice Over to complete before continuing")]
        [SerializeField] protected bool waitForVO = false;

        //add wait for vo that overrides stopvo

        [Tooltip("Sets the active Say dialog with a reference to a Say Dialog object in the scene. All story text will now display using this Say Dialog.")]
        [SerializeField] protected SayDialog setSayDialog;

        protected int executionCount;

        #region Public members

        /// <summary>
        /// Character that is speaking.
        /// </summary>
        public virtual Character _Character { get { return character; } }

        /// <summary>
        /// Portrait that represents speaking character.
        /// </summary>
        public virtual Sprite Portrait { get { return portrait; } set { portrait = value; } }

        /// <summary>
        /// Type this text in the previous dialog box.
        /// </summary>
        public virtual bool ExtendPrevious { get { return extendPrevious; } }

        public void Next()
        {

        }

        public override void OnEnter()
        {
            
            float timer = 1f;
            LeanTweenType orthoSizeTweenType = LeanTweenType.easeInOutQuad;
            LeanTweenType posTweenType = LeanTweenType.clamp;
            LeanTweenType rotTweenType = LeanTweenType.easeInOutQuad;


            var targetCamera = Camera.main;
            
            var cameraManager = FungusManager.Instance.CameraManager;
            var targetView = iCore.CurrentView;


            if (targetView != null)
            {  
                var a = targetView.SecondaryAspectRatio.x - targetView.PrimaryAspectRatio.x;
                var b = a * 0.6666f;
                var c = b * targetView.ViewSize / 2f;
                c = c < 2f ? c : 2f;
                Vector3 shiftPosition = new Vector3(0, 0, targetCamera.transform.position.z);
                if (character.PortraitsFace == FacingDirection.Left)
                {
                    shiftPosition = new Vector3(-c, 0, targetCamera.transform.position.z);
                }
                if (character.PortraitsFace == FacingDirection.Right)
                {
                    shiftPosition = new Vector3(c, 0, targetCamera.transform.position.z);
                }

                if (character.PortraitsFace != FacingDirection.None)
                {

                    Vector3 targetPosition = targetView.transform.position + shiftPosition;
                    Quaternion targetRotation = targetView.transform.rotation;
                    float targetSize = targetView.ViewSize;

                    //float distance = Vector3.Distance(targetPosition, targetCamera.transform.position);

                    //timer = distance / 5f;
                    // Debug.Log(timer);
                    // timer = 

                    //Debug.Log($"{targetPosition} - {targetCamera.transform.position}");
                    //if (targetPosition.x == targetCamera.transform.position.x)
                    //{
                    //    timer = 0;
                    //}

                    cameraManager.PanToPosition(targetCamera, targetPosition, targetRotation, targetSize, timer, delegate {
                        //if (true)
                        //{
                        //    Continue();
                        //}

                    }, orthoSizeTweenType, posTweenType, rotTweenType);
                }

            }



            //////////////////////////////////////
            if (!showAlways && executionCount >= showCount)
            {
                Continue();
                return;
            }

            executionCount++;

            // Override the active say dialog if needed
            if (character != null && character.SetSayDialog != null)
            {
                SayDialog.ActiveSayDialog = character.SetSayDialog;
            }

            if (setSayDialog != null)
            {
                SayDialog.ActiveSayDialog = setSayDialog;
            }

            var sayDialog = SayDialog.GetSayDialog();
            if (sayDialog == null)
            {
                Continue();
                return;
            }

            var flowchart = GetFlowchart();

            sayDialog.SetActive(true);

            sayDialog.SetCharacter(character);
            sayDialog.SetCharacterImage(portrait, character.Body, character.BodyPosition.x, character.BodyPosition.y);

            string displayText = storyText;

            var activeCustomTags = CustomTag.activeCustomTags;
            for (int i = 0; i < activeCustomTags.Count; i++)
            {
                var ct = activeCustomTags[i];
                displayText = displayText.Replace(ct.TagStartSymbol, ct.ReplaceTagStartWith);
                if (ct.TagEndSymbol != "" && ct.ReplaceTagEndWith != "")
                {
                    displayText = displayText.Replace(ct.TagEndSymbol, ct.ReplaceTagEndWith);
                }
            }

            string subbedText = flowchart.SubstituteVariables(displayText);

            sayDialog.Say(subbedText, !extendPrevious, waitForClick, fadeWhenDone, stopVoiceover, waitForVO, voiceOverClip, delegate {
                Continue();
            });
        }

        public override string GetSummary()
        {
            string namePrefix = "";
            if (character != null) 
            {
                namePrefix = character.NameText + ": ";
            }
            if (extendPrevious)
            {
                namePrefix = "EXTEND" + ": ";
            }
            return namePrefix + "\"" + storyText + "\"";
        }

        public override Color GetButtonColor()
        {
            if (storyText.Length > 170)
            {
                return new Color32(255, 0, 0, 255);
            }

            if (character != null)
            {
                if (portrait == null)
                {
                    return new Color32(255, 0, 0, 255);
                }

                if (setSayDialog == null)
                {
                    return new Color32(184, 210, 235, 255);
                }
                else
                {
                    return new Color32(184, 235, 210, 255);
                }
            }
            else
            {
                return new Color32(255, 0, 0, 255);
            }           
        }

        public override void OnReset()
        {
            executionCount = 0;
        }

        public override void OnStopExecuting()
        {
            var sayDialog = SayDialog.GetSayDialog();
            if (sayDialog == null)
            {
                return;
            }

            sayDialog.Stop();
        }

        #endregion

        #region ILocalizable implementation

        public virtual string GetStandardText()
        {
            return storyText;
        }

        public virtual void SetStandardText(string standardText)
        {
            storyText = standardText;
        }

        public virtual string GetDescription()
        {
            return description;
        }
        
        public virtual string GetStringId()
        {
            // String id for Say commands is SAY.<Localization Id>.<Command id>.[Character Name]
            string stringId = "SAY." + GetFlowchartLocalizationId() + "." + itemId + ".";
            if (character != null)
            {
                stringId += character.NameText;
            }

            return stringId;
        }

        #endregion
    }
}