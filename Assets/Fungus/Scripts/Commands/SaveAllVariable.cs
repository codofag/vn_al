// This code is part of the Fungus library (https://github.com/snozbot/fungus)
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;

namespace Fungus
{
    /// <summary>
    /// Save an Boolean, Integer, Float or String variable to persistent storage using a string key.
    /// The value can be loaded again later using the Load Variable command. You can also 
    /// use the Set Save Profile command to manage separate save profiles for multiple players.
    /// </summary>
    [CommandInfo("Variable", 
                 "Save All Variable", 
                 "Save an Boolean, Integer, Float or String variable to persistent storage using a string key. " +
                 "The value can be loaded again later using the Load Variable command. You can also " +
                 "use the Set Save Profile command to manage separate save profiles for multiple players.")]
    [AddComponentMenu("")]
    public class SaveAllVariable : Command
    {
        [Tooltip("Name of the saved value. Supports variable substition e.g. \"player_{$PlayerNumber}")]
        [SerializeField] protected string key = "";

        [SerializeField] protected bool EndEpisode = false;

        [Tooltip("Variable to read the value from. Only Boolean, Integer, Float and String are supported.")]
        [VariableProperty(typeof(BooleanVariable),
                          typeof(IntegerVariable), 
                          typeof(FloatVariable), 
                          typeof(StringVariable))]
        [HideInInspector]
        [SerializeField] protected Variable variable;

        #region Public members

        public override void OnEnter()
        {
            //if (key == "" ||
            //    variable == null)
            //{
            //    Continue();
            //    return;
            //}


            var flowchart = GetFlowchart();
            foreach (var item in flowchart.Variables)
            {
                // Prepend the current save profile (if any)
                string prefsKey = key + "_" + flowchart.SubstituteVariables(item.Key);

                System.Type variableType = item.GetType();

                if (variableType == typeof(BooleanVariable))
                {
                    BooleanVariable booleanVariable = item as BooleanVariable;
                    if (booleanVariable != null)
                    {
                        // PlayerPrefs does not have bool accessors, so just use int
                        PlayerPrefs.SetInt(prefsKey, booleanVariable.Value ? 1 : 0);
                    }
                }
                else if (variableType == typeof(IntegerVariable))
                {
                    IntegerVariable integerVariable = item as IntegerVariable;
                    if (integerVariable != null)
                    {
                        PlayerPrefs.SetInt(prefsKey, integerVariable.Value);
                    }
                }
                else if (variableType == typeof(FloatVariable))
                {
                    FloatVariable floatVariable = item as FloatVariable;
                    if (floatVariable != null)
                    {
                        PlayerPrefs.SetFloat(prefsKey, floatVariable.Value);
                    }
                }
                else if (variableType == typeof(StringVariable))
                {
                    StringVariable stringVariable = item as StringVariable;
                    if (stringVariable != null)
                    {
                        if (!EndEpisode)
                        {
                            if (item.Key == "NextBlock")
                            {
                                stringVariable.Value = ParentBlock.BlockName;
                            }
                        }

                        PlayerPrefs.SetString(prefsKey, stringVariable.Value);
                    }
                }

                flowchart.gameObject.GetComponent<Controller>().SaveGame();


                Continue();
            }
            
        }
        
        public override string GetSummary()
        {
            if (key.Length == 0)
            {
                return "Error: No stored value key selected";
            }

            //if (variable == null)
            //{
            //    return "Error: No variable selected";
            //}

            if (!EndEpisode)
            {
                return key;
            }
            else
            {
                return key + "+ EndEpisode";
            }
            
        }
        
        public override Color GetButtonColor()
        {

            return new Color32(255, 150, 255, 255);
        }

        public override bool HasReference(Variable in_variable)
        {
            return this.variable == in_variable || base.HasReference(in_variable);
        }

        #endregion
        #region Editor caches
#if UNITY_EDITOR
        protected override void RefreshVariableCache()
        {
            base.RefreshVariableCache();

            var f = GetFlowchart();

            f.DetermineSubstituteVariables(key, referencedVariables);
        }
#endif
        #endregion Editor caches
    }
}