// This code is part of the Fungus library (https://github.com/snozbot/fungus)
// It is released for free under the MIT open source license (https://github.com/snozbot/fungus/blob/master/LICENSE)

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Serialization;

namespace Fungus
{
    /// <summary>
    /// Sets the text property on a UI Text object and/or an Input Field object.
    /// </summary>
    [CommandInfo("UI", 
                 "Set Text", 
                 "Sets the text property on a UI Text object and/or an Input Field object.")]
    [AddComponentMenu("")]
    public class SetText : Command, ILocalizable 
    {
        [Tooltip("Text object to set text on. Can be a UI Text, Text Field or Text Mesh object.")]
        [SerializeField] protected GameObject targetTextObject;
        
        [Tooltip("String value to assign to the text object")]
        [FormerlySerializedAs("stringData")]
        [VariableProperty(typeof(BooleanVariable),
                          typeof(IntegerVariable),
                          typeof(FloatVariable),
                          typeof(StringVariable))]
        [SerializeField] protected Variable text;

        [Tooltip("Notes about this story text for other authors, localization, etc.")]
        [SerializeField] protected string description;
        [SerializeField] protected string PreScript;
        [SerializeField] protected string PostScript;

        #region Public members
        string newText = "";
        public override void OnEnter()
        {
            newText = PreScript + GenerateText() + PostScript;
            var flowchart = GetFlowchart();

            if (targetTextObject == null)
            {
                Continue();
                return;
            }

            TextAdapter textAdapter = new TextAdapter();
            textAdapter.InitFromGameObject(targetTextObject);

            if (textAdapter.HasTextObject())
            {
                textAdapter.Text = newText;
            }

            Continue();
        }

        string GenerateText()
        {
            System.Type variableType = text.GetType();
            if (variableType == typeof(StringVariable))
            {
                StringVariable stringVariable = text as StringVariable;
                if (stringVariable != null)
                {
                    return stringVariable.Value;
                }
            }

            if (variableType == typeof(IntegerVariable))
            {
                IntegerVariable stringVariable = text as IntegerVariable;
                if (stringVariable != null)
                {
                    return stringVariable.Value.ToString();
                }
            }

            if (variableType == typeof(BooleanVariable))
            {
                BooleanVariable stringVariable = text as BooleanVariable;
                if (stringVariable != null)
                {
                    return stringVariable.Value.ToString();
                }
            }

            if (variableType == typeof(FloatVariable))
            {
                FloatVariable stringVariable = text as FloatVariable;
                if (stringVariable != null)
                {
                    return stringVariable.Value.ToString();
                }
            }

            return "";
        }
        
        public override string GetSummary()
        {
            

            if (targetTextObject != null)
            {
                return targetTextObject.name + " : ";// + GenerateText();
            }
            
            return "Error: No text object selected";
        }
        
        public override Color GetButtonColor()
        {
            return new Color32(235, 191, 217, 255);
        }

        public override bool HasReference(Variable variable)
        {
            return  base.HasReference(variable);
        }

        #endregion


        #region Editor caches
#if UNITY_EDITOR
        protected override void RefreshVariableCache()
        {
            base.RefreshVariableCache();

            var f = GetFlowchart();

            f.DetermineSubstituteVariables(newText, referencedVariables);
        }
#endif
        #endregion Editor caches

        #region ILocalizable implementation

        public virtual string GetStandardText()
        {
            return newText;
        }

        public virtual void SetStandardText(string standardText)
        {
            newText = standardText;
        }

        public virtual string GetDescription()
        {
            return description;
        }
        
        public virtual string GetStringId()
        {
            // String id for Set Text commands is SETTEXT.<Localization Id>.<Command id>
            return "SETTEXT." + GetFlowchartLocalizationId() + "." + itemId;
        }

        #endregion

        #region Backwards compatibility

        // Backwards compatibility with Fungus v2.1.2
        [HideInInspector]
        [FormerlySerializedAs("textObject")]
        public Text _textObjectObsolete;
        protected virtual void OnEnable()
        {
            if (_textObjectObsolete != null)
            {
                targetTextObject = _textObjectObsolete.gameObject;
            }
        }

        #endregion
    }    
}